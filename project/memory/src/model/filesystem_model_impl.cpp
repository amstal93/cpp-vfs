/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/model/filesystem_model_impl.hpp"

#include "ih/entries/directory.hpp"
#include "ih/root.hpp"

#include "src/entries/visitors/directory_creator_impl.hpp"
#include "src/entries/visitors/directory_iterator_impl.hpp"
#include "src/entries/directory_entry_impl.hpp"

#include <cassert>

namespace b110011 {
namespace filesystem {
namespace memory {

FileSystemModelImpl::FileSystemModelImpl ( std::shared_ptr< Root > _pRoot )
    :   m_pRoot{ _pRoot }
{}

FileSystemModelImpl::~FileSystemModelImpl () = default;

std::uintmax_t
FileSystemModelImpl::getFileSize (
    std::filesystem::path const & _path,
    std::error_code & _ec
)  const
{
    assert( m_pRoot );
    auto const * pEntry = m_pRoot->getEntry( _path, _ec );

    std::uintmax_t result = -1;
    if ( !_ec )
    {
        assert( pEntry );
        switch ( pEntry->getType() )
        {
            case std::filesystem::file_type::regular:
                result = pEntry->getSize();
                break;

            case std::filesystem::file_type::directory:
                _ec = std::make_error_code( std::errc::is_a_directory );
                break;

            default:
                _ec = std::make_error_code( std::errc::not_supported );
                break;
        }
    }

    return result;
}

std::filesystem::file_time_type
FileSystemModelImpl::getLastWriteTime ( std::filesystem::path const & _path ) const
{
    assert( m_pRoot );
    return m_pRoot->getEntry( _path ).getLastModificationTime();
}

void
FileSystemModelImpl::setLastWriteTime (
        std::filesystem::path const & _path
    ,	std::filesystem::file_time_type _newTime
)
{
    assert( m_pRoot );
    m_pRoot->getEntry( _path ).setLastModificationTime( _newTime );
}

std::filesystem::space_info
FileSystemModelImpl::getSpaceInfo ( std::filesystem::path const & _path ) const
{
    std::filesystem::space_info result;

    //result.capacity =
    //result.available = result.free = result.capacity - m_pRoot->getSize();

    return result;
}

bool
FileSystemModelImpl::isDirectory ( std::filesystem::path const & _path ) const
{
    assert( m_pRoot );
    return m_pRoot->getEntry( _path ).getType() == std::filesystem::file_type::directory;
}

bool
FileSystemModelImpl::isRegularFile ( std::filesystem::path const & _path ) const
{
    assert( m_pRoot );
    return m_pRoot->getEntry( _path ).getType() == std::filesystem::file_type::regular;
}

bool
FileSystemModelImpl::exists ( std::filesystem::path const & _path ) const
{
    return m_pRoot->exists( _path );
}

bool
FileSystemModelImpl::forEachDirectoryEntry (
    std::filesystem::path const & _directoryPath,
    bool _useRecursiveTraversal,
    DirectoryEntryCallback _callback
) const
{
    assert( m_pRoot );
    return DirectoryIteratorImpl{}.iterate(
        m_pRoot->getEntry( _directoryPath ),
        [ _callback ] ( std::shared_ptr< Entry const > _pEntry )
        {
            DirectoryEntryImpl entry{ _pEntry };
            return _callback( entry );
        }
    );
}

void
FileSystemModelImpl::rename (
    std::filesystem::path const & _oldPath,
    std::filesystem::path const & _newPath
)
{
    auto * pParent{ m_pRoot->getEntry( _oldPath ).takeParentFolderIfExist() };
    assert( pParent );

    pParent->rename( _oldPath.filename(), _newPath.filename() );
}

bool
FileSystemModelImpl::remove ( std::filesystem::path const & _path )
{
    auto * pParent{ m_pRoot->getEntry( _path ).takeParentFolderIfExist() };
    assert( pParent );

    pParent->remove( _path.filename() );
}

bool
FileSystemModelImpl::createDirectory ( std::filesystem::path const & _path )
{
    auto * pParent{ m_pRoot->getEntry( _path ).takeParentFolderIfExist() };
    assert( pParent );

    DirectoryCreatorImpl creator;
    return creator.create( *pParent, _path.filename() );
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
