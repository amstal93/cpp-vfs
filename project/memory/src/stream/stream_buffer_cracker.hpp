/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_STREAM_STREAMBUFFERCRACKER_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_STREAM_STREAMBUFFERCRACKER_HPP__

#include <streambuf>

namespace b110011 {
namespace filesystem {
namespace memory {

template< class _Elem, class _Traits >
class StreamBufferCracker final
    :   private std::basic_streambuf< _Elem, _Traits >
{

    using BaseClass = std::basic_streambuf< _Elem, _Traits >;

public:

    StreamBufferCracker ( BaseClass const & _streamBuffer )
        :   BaseClass{ _streamBuffer }
    {}

    // Read buffer

    using BaseClass::eback;

    using BaseClass::gptr;

    using BaseClass::egptr;

    // Write buffer

    using BaseClass::pbase;

    using BaseClass::pptr;

    using BaseClass::epptr;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_STREAM_STREAMBUFFERCRACKER_HPP__
