/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/file_impl.hpp"

#include "ih/entries/visitors/modifiable_visitor.hpp"
#include "ih/entries/visitors/read_only_visitor.hpp"

#include "src/stream/stream_buffer_cracker.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

FileImpl::FileImpl ( Directory * _pParent, std::filesystem::path _name )
    :   BaseClass{
            _pParent,
            std::move( _name ),
            std::filesystem::file_type::regular
        }
    ,   m_descriptorsManager{ m_buffer }
{}

std::uintmax_t
FileImpl::getSize () const
{
    StreamBufferCracker cracker{ m_buffer };
    return cracker.epptr() - cracker.pbase();
}

FileStream
FileImpl::createStream ( std::ios::openmode _mode, std::error_code _ec )
{
    return m_descriptorsManager.createStream( _mode, _ec );
}

InputFileStream
FileImpl::createInputStream ( std::ios::openmode _mode, std::error_code _ec )
{
    return m_descriptorsManager.createInputStream( _mode, _ec );
}

OutputFileStream
FileImpl::createOutputStream ( std::ios::openmode _mode, std::error_code _ec )
{
    return m_descriptorsManager.createOutputStream( _mode, _ec );
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
