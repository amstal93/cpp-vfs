/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_FILEIMPL_HPP__
#define __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_FILEIMPL_HPP__

#include "ih/entries/file.hpp"

#include "src/entries/entry_base_impl.hpp"
#include "src/stream/file_descriptors_manager.hpp"

#include <sstream>

namespace b110011 {
namespace filesystem {
namespace memory {

class FileImpl final
    :   public EntryBaseImpl< File >
{

public:

    FileImpl ( Directory * _pParent, std::filesystem::path _name );


    std::uintmax_t getSize () const override;


    FileStream createStream (
        std::ios::openmode _mode,
        std::error_code _ec
    ) override;

    InputFileStream createInputStream (
        std::ios::openmode _mode,
        std::error_code _ec
    ) override;

    OutputFileStream createOutputStream (
        std::ios::openmode _mode,
        std::error_code _ec
    ) override;

private:

    using BaseClass = EntryBaseImpl< File >;

    std::stringbuf m_buffer;

    FileDescriptorsManager m_descriptorsManager;

};

} // namespace memory
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_MEMORY_SRC_ENTRIES_FILEIMPL_HPP__
