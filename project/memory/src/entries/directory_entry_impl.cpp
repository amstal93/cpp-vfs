/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/directory_entry_impl.hpp"

#include "ih/entries/entry.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

DirectoryEntryImpl::DirectoryEntryImpl ( std::shared_ptr< Entry const > _pEntry ) noexcept
    :   m_pWeakEntry{ _pEntry }
    ,   m_cachedLastWriteTime{ _pEntry->getLastModificationTime() }
    ,   m_cachedType{ _pEntry->getType() }
{}

std::filesystem::path const &
DirectoryEntryImpl::getPath () const
{
    if ( !m_cachedPath.empty() )
    {
        return m_cachedPath;
    }

    if ( auto pEntry{ m_pWeakEntry.lock() } )
    {
        m_cachedPath = pEntry->getFullPath();
    }

    return m_cachedPath;
}

std::filesystem::file_time_type
DirectoryEntryImpl::getLastWriteTime ( std::error_code & _ec ) const noexcept
{
    _ec.clear();
    return m_cachedLastWriteTime;
}

bool
DirectoryEntryImpl::isBlockFile ( std::error_code & _ec ) const noexcept
{
    return unsupported( _ec );
}

bool
DirectoryEntryImpl::isCharacterFile ( std::error_code & _ec ) const noexcept
{
    return unsupported( _ec );
}

bool
DirectoryEntryImpl::isDirectory ( std::error_code & _ec ) const noexcept
{
    _ec.clear();
    return m_cachedType == std::filesystem::file_type::directory;
}

bool
DirectoryEntryImpl::isFifo ( std::error_code & _ec ) const noexcept
{
    return unsupported( _ec );
}

bool
DirectoryEntryImpl::isOther ( std::error_code & _ec ) const noexcept
{
    return unsupported( _ec );
}

bool
DirectoryEntryImpl::isRegularFile ( std::error_code & _ec ) const noexcept
{
    _ec.clear();
    return m_cachedType == std::filesystem::file_type::regular;
}

bool
DirectoryEntryImpl::isSocket ( std::error_code & _ec ) const noexcept
{
    return unsupported( _ec );
}

bool
DirectoryEntryImpl::isSymlink ( std::error_code & _ec ) const noexcept
{
    return unsupported( _ec );
}

bool
DirectoryEntryImpl::exists ( std::error_code & _ec ) const noexcept
{
    _ec.clear();
    return m_pWeakEntry.use_count();
}

DirectoryEntryImpl::operator std::filesystem::path const & () const
{
    return getPath();
}

bool
DirectoryEntryImpl::unsupported ( std::error_code & _ec ) noexcept
{
    _ec = std::make_error_code( std::errc::not_supported );
    return false;
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
