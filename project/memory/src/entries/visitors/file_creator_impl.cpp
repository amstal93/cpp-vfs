/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/visitors/file_creator_impl.hpp"

#include "src/entries/visitors/entry_cast_impl.hpp"
#include "src/entries/directory_impl.hpp"
#include "src/entries/file_impl.hpp"

#include <cassert>

namespace b110011 {
namespace filesystem {
namespace memory {

FileCreatorImpl::FileCreatorImpl ( Entry & _parentEntry ) noexcept
    :   m_parentEntry{ _parentEntry }
    ,   m_pResult{ nullptr }
{}

std::error_code
FileCreatorImpl::getError () const noexcept
{
    return m_error;
}

File *
FileCreatorImpl::create ( std::filesystem::path const & _name )
{
    m_pFileName = &_name;
    m_parentEntry.accept( *this );

    return m_pResult;
}

void
FileCreatorImpl::visit ( Directory & _directory )
{
    auto name{ m_pFileName->filename() };
    if ( auto pEntry{ _directory.find( name ) } )
    {
        switch ( pEntry->getType() )
        {
            case std::filesystem::file_type::regular:
                m_pResult = EntryCastImpl< File >{}( *pEntry );
                m_error = std::make_error_code( std::errc::file_exists );
                return;

            case std::filesystem::file_type::directory:
                m_error = std::make_error_code( std::errc::is_a_directory );
                return;

            default:
                // TODO: Find a better error code.
                m_error = std::make_error_code( std::errc::not_supported );
                return;
        }
    }

    auto pNewFile{ std::make_shared< FileImpl >( &_directory, std::move( name ) ) };
    _directory.add( pNewFile );

    m_pResult = pNewFile.get();
}

void
FileCreatorImpl::visit ( File & )
{
    m_error = std::make_error_code( std::errc::not_a_directory );
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
