/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/visitors/entry_searcher_impl.hpp"

#include "ih/entries/directory.hpp"

#ifdef _WIN32
#include <cstdlib>
#else
#include <linux/limits.h>
#endif

namespace b110011 {
namespace filesystem {
namespace memory {

#ifdef _WIN32
static constexpr std::size_t s_maxFileNameLength = _MAX_FNAME;
static constexpr std::size_t s_maxPathLength = _MAX_FNAME;
#else
static constexpr std::size_t s_maxFileNameLength = NAME_MAX;
static constexpr std::size_t s_maxPathLength = PATH_MAX;
#endif

EntrySearcherImpl::EntrySearcherImpl ( Entry & _startingEntry ) noexcept
    :   m_pCurrentEntry{ &_startingEntry }
    ,   m_error{ std::make_error_code( std::errc::no_such_file_or_directory ) }
{}

std::error_code
EntrySearcherImpl::getError () const noexcept
{
    return m_error;
}

Entry *
EntrySearcherImpl::find ( std::filesystem::path const & _path )
{
    if ( _path.empty() )
    {
        m_error = std::make_error_code( std::errc::no_such_file_or_directory );
        return nullptr;
    }
    else if ( _path.native().length() >= s_maxPathLength )
    {
        m_error = std::make_error_code( std::errc::filename_too_long );
        return nullptr;
    }
    else if ( _path.filename().native().length() >= s_maxPathLength )
    {
        m_error = std::make_error_code( std::errc::filename_too_long );
        return nullptr;
    }

    if ( m_pCurrentEntry->getName() == _path )
    {
        return m_pCurrentEntry;
    }

    auto itEnd{ _path.end() };
    for ( m_itCurrentElement = _path.begin(); m_itCurrentElement != itEnd; ++m_itCurrentElement )
    {
        if ( *m_itCurrentElement == "." )
        {
            continue;
        }

        if ( *m_itCurrentElement == ".." )
        {
            m_pCurrentEntry = m_pCurrentEntry->takeParentDirectory();
        }

        if ( !m_pCurrentEntry )
        {
            break;
        }

        m_pCurrentEntry->accept( *this );
    }

    return m_pCurrentEntry;
}

void
EntrySearcherImpl::visit ( Directory & _directory )
{
    m_pCurrentEntry = _directory.find( *m_itCurrentElement );
}

void
EntrySearcherImpl::visit ( File & )
{
    m_pCurrentEntry = nullptr;
}

} // namespace memory
} // namespace filesystem
} // namespace b110011
