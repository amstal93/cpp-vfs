/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/entries/entry_base_impl.hpp"

#include "ih/entries/visitors/modifiable_visitor.hpp"
#include "ih/entries/visitors/read_only_visitor.hpp"
#include "ih/entries/directory.hpp"
#include "ih/entries/file.hpp"

namespace b110011 {
namespace filesystem {
namespace memory {

template< class _Entry >
EntryBaseImpl< _Entry >::EntryBaseImpl (
    Directory * _pParent,
    std::filesystem::path _name,
    std::filesystem::file_type _type
)
    :   m_parent{ _pParent }
    ,   m_lastModificationTime{ std::filesystem::file_time_type::clock::now() }
    ,   m_type{ _type }
    ,   m_name{ std::move( _name ) }
{}

template< class _Entry >
std::uintmax_t
EntryBaseImpl< _Entry >::getSize () const
{
    return 0;
}

template< class _Entry >
std::filesystem::path const &
EntryBaseImpl< _Entry >::getName () const noexcept
{
    return m_name;
}

template< class _Entry >
void
EntryBaseImpl< _Entry >::setName ( std::filesystem::path const & _newName )
{
    m_name = _newName;
}

template< class _Entry >
std::filesystem::path
EntryBaseImpl< _Entry >::getFullPath () const
{
    std::filesystem::path result{ getName() };

    auto const * pParent{ getParentDirectory() };
    while ( ( pParent = pParent->getParentDirectory() ) != nullptr )
    {
        result = pParent->getName() / result;
    }

    return result;
}

template< class _Entry >
Directory const *
EntryBaseImpl< _Entry >::getParentDirectory () const noexcept
{
    return m_parent;
}

template< class _Entry >
Directory *
EntryBaseImpl< _Entry >::takeParentDirectory () noexcept
{
    return m_parent;
}

template< class _Entry >
void
EntryBaseImpl< _Entry >::setParentDirectory ( Directory * _pNewParent ) noexcept
{
    m_parent = _pNewParent;
}

template< class _Entry >
std::filesystem::file_time_type
EntryBaseImpl< _Entry >::getLastModificationTime () const noexcept
{
    return m_lastModificationTime;
}

template< class _Entry >
void
EntryBaseImpl< _Entry >::setLastModificationTime (
    std::filesystem::file_time_type _newTime
) noexcept
{
    m_lastModificationTime = _newTime;
}

template< class _Entry >
std::filesystem::file_type
EntryBaseImpl< _Entry >::getType () const noexcept
{
    return m_type;
}

template< class _Entry >
void
EntryBaseImpl< _Entry >::accept ( ModifiableVisitor & _visitor )
{
    _visitor.visit( *this );
}

template< class _Entry >
void
EntryBaseImpl< _Entry >::accept ( ReadOnlyVisitor & _visitor ) const
{
    _visitor.visit( *this );
}

template class EntryBaseImpl< Directory >;
template class EntryBaseImpl< File >;

} // namespace memory
} // namespace filesystem
} // namespace b110011
