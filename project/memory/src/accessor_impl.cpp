/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/accessor_impl.hpp"

#include "src/model/filesystem_model_impl.hpp"
#include "src/model/root_impl.hpp"
#include "src/stream/file_factory_impl.hpp"

#include <cassert>

namespace b110011 {
namespace filesystem {
namespace memory {

AccessorImpl::AccessorImpl ()
    :   m_pRoot{ std::make_shared< RootImpl >() }
    ,   m_pModel{ std::make_unique< FileSystemModelImpl >( m_pRoot ) }
{}

AccessorImpl::~AccessorImpl () = default;

FileFactory &
AccessorImpl::takeFileFactory ()
{
    assert( m_pRoot );
    return *m_pRoot;
}

FileSystemModel const &
AccessorImpl::getFileSystem () const
{
    assert( m_pModel );
    return *m_pModel;
}

FileSystemModel &
AccessorImpl::takeFileSystem ()
{
    assert( m_pModel );
    return *m_pModel;
}

} // namespace memory

std::unique_ptr< Accessor >
Accessor::createUniqueAccessor ()
{
    return std::make_unique< memory::AccessorImpl >();
}

std::shared_ptr< Accessor >
Accessor::createSharedAccessor ()
{
    return std::make_shared< memory::AccessorImpl >();
}

} // namespace filesystem
} // namespace b110011
