/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_NATIVE_SRC_ACCESSORIMPL_HPP__
#define __B110011_FILESYSTEM_NATIVE_SRC_ACCESSORIMPL_HPP__

#include "b110011/filesystem/accessor.hpp"

namespace b110011 {
namespace filesystem {
namespace native {

class AccessorImpl final
    :   public Accessor
{

public:

    AccessorImpl ();

    ~AccessorImpl () override;


    FileFactory & takeFileFactory () override;


    FileSystemModel const & getFileSystem () const override;

    FileSystemModel & takeFileSystem () override;

private:

    std::unique_ptr< FileFactory > m_pFilesFactory;
    std::unique_ptr< FileSystemModel > m_pModel;

};

} // namespace native
} // namespace filesystem
} // namespace b110011

#endif // __B110011_FILESYSTEM_NATIVE_SRC_ACCESSORIMPL_HPP__
