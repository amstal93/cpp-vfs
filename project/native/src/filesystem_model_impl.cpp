/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#include "src/filesystem_model_impl.hpp"

#include "src/entries/directory_entry_impl.hpp"

namespace b110011 {
namespace filesystem {
namespace native {

std::uintmax_t
FileSystemModelImpl::getFileSize (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    return std::filesystem::file_size( _path, _ec );
}

std::filesystem::file_time_type
FileSystemModelImpl::getLastWriteTime (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    return std::filesystem::last_write_time( _path, _ec );
}

void
FileSystemModelImpl::setLastWriteTime (
    std::filesystem::path const & _path,
    std::filesystem::file_time_type _newTime,
    std::error_code & _ec
)
{
    std::filesystem::last_write_time( _path, _newTime, _ec );
}

std::filesystem::space_info
FileSystemModelImpl::getSpaceInfo (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    return std::filesystem::space( _path, _ec );
}

bool
FileSystemModelImpl::isDirectory (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    return std::filesystem::is_directory( _path, _ec );
}

bool
FileSystemModelImpl::isRegularFile (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    return std::filesystem::is_regular_file( _path, _ec );
}

bool
FileSystemModelImpl::exists (
    std::filesystem::path const & _path,
    std::error_code & _ec
) const
{
    return std::filesystem::exists( _path, _ec );
}

bool
FileSystemModelImpl::forEachDirectoryEntry (
    std::filesystem::path const & _path,
    bool _useRecursiveTraversal,
    DirectoryEntryCallback _callback
) const
{
    if ( _useRecursiveTraversal )
    {
        using iterator = std::filesystem::recursive_directory_iterator;
        return forEachDirectoryEntry< iterator >( _path, std::move( _callback ) );
    }
    else
    {
        using iterator = std::filesystem::directory_iterator;
        return forEachDirectoryEntry< iterator >( _path, std::move( _callback ) );
    }
}

template< typename _IteratorT >
bool
FileSystemModelImpl::forEachDirectoryEntry (
    std::filesystem::path const & _directoryPath,
    DirectoryEntryCallback _callback
)
{
    for ( auto const & entry : _IteratorT{ _directoryPath } )
    {
        DirectoryEntryImpl wrapper{ entry };
        if ( !_callback( wrapper ) )
        {
            return false;
        }
    }

    return true;
}

void
FileSystemModelImpl::rename (
    std::filesystem::path const & _oldPath,
    std::filesystem::path const & _newPath,
    std::error_code & _ec
)
{
    std::filesystem::rename( _oldPath, _newPath, _ec );
}

bool
FileSystemModelImpl::remove (
    std::filesystem::path const & _path,
    std::error_code & _ec
)
{
    return std::filesystem::remove( _path, _ec );
}

bool
FileSystemModelImpl::createDirectory (
    std::filesystem::path const & _path,
    std::error_code & _ec
)
{
    return std::filesystem::create_directory( _path, _ec );
}

} // namespace native
} // namespace filesystem
} // namespace b110011
