# cpp-vfs: Simple virtual filesystem.
#
# Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

project(native-fs
  LANGUAGES
    CXX
  DESCRIPTION
    "Implementation of native filesystem."
)

#------------------------------------------------------------------------------#
# Project files                                                                #
#------------------------------------------------------------------------------#

set(HEADERS
  src/stream/stream_factory_proxy.hpp
  src/stream/file_factory_impl.hpp
  src/filesystem_model_impl.hpp
  src/entries/directory_entry_impl.hpp
  src/accessor_impl.hpp
)

set(SOURCE
  src/stream/stream_factory_proxy.cpp
  src/stream/file_factory_impl.cpp
  src/filesystem_model_impl.cpp
  src/entries/directory_entry_impl.cpp
  src/accessor_impl.cpp
)

#------------------------------------------------------------------------------#
# Project                                                                      #
#------------------------------------------------------------------------------#

add_library("${PROJECT_NAME}" ${HEADERS} ${SOURCE})
add_library(B110011::RealFileSystem ALIAS ${PROJECT_NAME})

source_group("api" FILES ${API_HEADERS})

target_init("${PROJECT_NAME}")

#------------------------------------------------------------------------------#
# Project directories                                                          #
#------------------------------------------------------------------------------#

target_include_directories("${PROJECT_NAME}"
  PUBLIC
    $<BUILD_INTERFACE:${CMAKE_SOURCE_DIR}/include>
  PRIVATE
    "${PROJECT_SOURCE_DIR}"
)

#------------------------------------------------------------------------------#
# Project precompile headers                                                   #
#------------------------------------------------------------------------------#

target_precompile_headers("${PROJECT_NAME}"
  PRIVATE
    <filesystem>
    <functional>
    <memory>
    <string>
    <utility>
)
