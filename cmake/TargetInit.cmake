include(cmake/build/CompilerWarnings.cmake)
include(cmake/utils/Sanitizers.cmake)
include(cmake/utils/TimeTrace.cmake)

function(target_init project_name)

  set_target_properties(${project_name}
    PROPERTIES
      CXX_STANDARD 17
      CXX_STANDARD_REQUIRED YES
      CXX_EXTENSIONS NO
  )

  # Standard compiler warnings
  set_project_warnings(${project_name})

  # Code coverage
  enable_coverage(${project_name})

  # Sanitizer options if supported by compiler
  enable_sanitizers(${project_name})

  # Time trace options if supported by compiler
  enable_time_trace(${project_name})

endfunction()
