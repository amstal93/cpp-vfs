/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_FILESYSTEMMODEL_HPP__
#define __B110011_FILESYSTEM_FILESYSTEMMODEL_HPP__

#include "b110011/noncopyable.hpp"

#include <filesystem>
#include <functional>

namespace b110011 {
namespace filesystem {

class DirectoryEntry;

class FileSystemModel
    :   private NonCopyable
{

public:

    [[nodiscard]]
    std::uintmax_t getFileSize ( std::filesystem::path const & _path ) const;

    [[nodiscard]]
    virtual std::uintmax_t getFileSize (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;


    [[nodiscard]]
    std::filesystem::file_time_type getLastWriteTime (
            std::filesystem::path const & _path
    ) const;

    [[nodiscard]]
    virtual std::filesystem::file_time_type getLastWriteTime (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;

    void setLastWriteTime (
        std::filesystem::path const & _path,
        std::filesystem::file_time_type _newTime
    );

    virtual void setLastWriteTime (
        std::filesystem::path const & _path,
        std::filesystem::file_time_type _newTime,
        std::error_code & _ec
    ) = 0;


    [[nodiscard]]
    std::filesystem::space_info getSpaceInfo (
        std::filesystem::path const & _path
    ) const;

    [[nodiscard]]
    virtual std::filesystem::space_info getSpaceInfo (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;



    [[nodiscard]]
    bool isDirectory ( std::filesystem::path const & _path ) const;

    [[nodiscard]]
    virtual bool isDirectory (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;


    [[nodiscard]]
    bool isRegularFile ( std::filesystem::path const & _path ) const;

    [[nodiscard]]
    virtual bool isRegularFile (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;


    [[nodiscard]]
    bool exists ( std::filesystem::path const & _path ) const;

    [[nodiscard]]
    virtual bool exists (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) const = 0;


    using DirectoryEntryCallback = std::function< bool ( DirectoryEntry const & ) >;

    virtual bool forEachDirectoryEntry (
        std::filesystem::path const & _directoryPath,
        bool _useRecursiveTraversal,
        DirectoryEntryCallback _callback
    ) const = 0;


    void rename (
        std::filesystem::path const & _oldPath,
        std::filesystem::path const & _newPath
    );

    virtual void rename (
        std::filesystem::path const & _oldPath,
        std::filesystem::path const & _newPath,
        std::error_code & _ec
    ) = 0;


    bool remove ( std::filesystem::path const & _path );

    virtual bool remove (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) = 0;


    bool createDirectory ( std::filesystem::path const & _path );

    virtual bool createDirectory (
        std::filesystem::path const & _path,
        std::error_code & _ec
    ) = 0;

private:

    template< typename _ResultT >
    using Callback = _ResultT ( FileSystemModel::* ) (
        std::filesystem::path const &,
        std::error_code &
    ) const;

    template< typename _ResultT >
    _ResultT invoke (
        Callback< _ResultT > && _callback,
        std::filesystem::path const & _path,
        char const * _error
    ) const;

    template< typename _ResultT >
    bool checkType (
        Callback< _ResultT > && _callback,
        std::filesystem::path const & _path
    ) const;

};

} // namespace filesystem
} // namespace b110011

#include "b110011/filesystem/filesystem_model.ipp"

#endif // __B110011_FILESYSTEM_FILESYSTEMMODEL_HPP__
