/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_STREAM_FILEFACTORY_HPP__
#define __B110011_FILESYSTEM_STREAM_FILEFACTORY_HPP__

#include "b110011/filesystem/stream/stream.hpp"
#include "b110011/noncopyable.hpp"

#include <filesystem>
#include <fstream>

namespace b110011 {
namespace filesystem {

class FileFactory
    :   private NonCopyable
{

public:

    [[nodiscard]]
    FileStream createFile (
        std::filesystem::path const & _path,
        std::ios::openmode _mode
    );

    [[nodiscard]]
    virtual FileStream createFile (
        std::filesystem::path const & _path,
        std::ios::openmode _mode,
        std::error_code & _ec
    ) = 0;


    [[nodiscard]]
    InputFileStream createInputFile (
        std::filesystem::path const & _path,
        std::ios::openmode _mode
    );

    [[nodiscard]]
    virtual InputFileStream createInputFile (
        std::filesystem::path const & _path,
        std::ios::openmode _mode,
        std::error_code & _ec
    ) = 0;


    [[nodiscard]]
    OutputFileStream createOutputFile (
        std::filesystem::path const & _path,
        std::ios::openmode _mode
    );

    [[nodiscard]]
    virtual OutputFileStream createOutputFile (
        std::filesystem::path const & _path,
        std::ios::openmode _mode,
        std::error_code & _ec
    ) = 0;

};

} // namespace filesystem
} // namespace b110011

#include "b110011/filesystem/stream/file_factory.ipp"

#endif // __B110011_FILESYSTEM_STREAM_FILEFACTORY_HPP__
