/*
cpp-vfs: Simple virtual filesystem.

Copyright 2021 by Serhii Zinchenko <zinchenko.serhii@pm.me>.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

#ifndef __B110011_FILESYSTEM_ENTRIES_DIRECTORYENTRY_HPP__
#define __B110011_FILESYSTEM_ENTRIES_DIRECTORYENTRY_HPP__

#include "b110011/noncopyable.hpp"

#include <filesystem>

namespace b110011 {
namespace filesystem {

class DirectoryEntry
    :   private NonCopyable
{

public:

    [[nodiscard]]
    virtual std::filesystem::path const & getPath () const = 0;


    [[nodiscard]]
    std::filesystem::file_time_type getLastWriteTime () const;

    [[nodiscard]]
    virtual std::filesystem::file_time_type getLastWriteTime (
        std::error_code & _ec
    ) const = 0;


    [[nodiscard]]
    bool isBlockFile () const;

    [[nodiscard]]
    virtual bool isBlockFile ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool isCharacterFile () const;

    [[nodiscard]]
    virtual bool isCharacterFile ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool isDirectory () const;

    [[nodiscard]]
    virtual bool isDirectory ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool isFifo () const;

    [[nodiscard]]
    virtual bool isFifo ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool isOther () const;

    [[nodiscard]]
    virtual bool isOther ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool isRegularFile () const;

    [[nodiscard]]
    virtual bool isRegularFile ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool isSocket () const;

    [[nodiscard]]
    virtual bool isSocket ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool isSymlink () const;

    [[nodiscard]]
    virtual bool isSymlink ( std::error_code & _ec ) const = 0;


    [[nodiscard]]
    bool exists () const;

    [[nodiscard]]
    virtual bool exists ( std::error_code & _ec ) const = 0;


    virtual operator std::filesystem::path const & () const = 0;


    bool operator == ( DirectoryEntry const & _rhs ) const noexcept;

    #if __cpp_lib_three_way_comparison
    std::strong_ordering operator <=> ( DirectoryEntry const & _rhs ) const noexcept;
    #else
    bool operator != ( DirectoryEntry const & _rhs ) const noexcept;


    bool operator < ( DirectoryEntry const & _rhs ) const noexcept;

    bool operator <= ( DirectoryEntry const & _rhs ) const noexcept;


    bool operator > ( DirectoryEntry const & _rhs ) const noexcept;

    bool operator >= ( DirectoryEntry const & _rhs ) const noexcept;
    #endif

private:

    template< typename _ResultT >
    using Callback = _ResultT ( DirectoryEntry::* ) ( std::error_code & ) const;

    template< typename _ResultT >
    _ResultT invoke ( Callback< _ResultT > && _callback, char const * _error ) const;

    template< typename _ResultT >
    bool checkType ( Callback< _ResultT > && _callback ) const;

};

} // namespace filesystem
} // namespace b110011

#include "b110011/filesystem/entries/directory_entry.ipp"

#endif // __B110011_FILESYSTEM_ENTRIES_DIRECTORYENTRY_HPP__
